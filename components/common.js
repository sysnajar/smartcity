var api_host = "https://hjkl.ninja/dapi/"

function show_error(msg)
{

  alert('Error '+ (msg || 'N/A'))

}

 function upload(ele, successFn){
     
      var file = ele.files[0];
      var fileReader = new FileReader();
      fileReader.onloadend = function (e) {
        var arrayBuffer = e.target.result;
        var fileType = "image/png"//$('#file-type').val();
        var blob = blobUtil.arrayBufferToBlob(arrayBuffer, fileType)
          console.log('here is a blob', blob); 
          upload_blob(blob, successFn)
      };
      fileReader.readAsArrayBuffer(file);
    }


    function upload_blob(blob, successFn){
     var form = document.getElementById('upload_form');
     var formdata = new FormData(form);
     formdata.append('myfile', blob); 
      var url = api_host + 'upload_depa'

      $.ajax({
       url: url,
       type: "POST",
       data: formdata,
       processData: false,
       contentType: false,
       success: function (result) {  
        successFn(JSON.parse(result)) 
       },
       error: function (error) {
        console.log("Something went wrong!");
       }
      })
      
     } 

     

//return array of selected_items_id eg : ["1", "5"]
function selected_items_id(all_items)
{
  return all_items.filter(function(f){return f.selected==true}).map(function(f){return f._id})
}

function set_selected_items(all_items, selected_items_id)
{
     if(selected_items_id && selected_items_id.length)
        { for(var i=0;i<selected_items_id.length;i++)
             { 
                 var item = all_items.find(function(item){ return item._id == selected_items_id[i]  })
                 if(item)
                   item.selected = true
             } 
        } 
}


 function check_del_item(_id , checked, app)
{
  var map = app.delete_ids
  map[_id] = checked

  console.log('delete_ids:'+ JSON.stringify(map))
}

 function gup( name, url ) {
 if (!url) url = location.href;
 name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
 var regexS = "[\\?&]"+name+"=([^&#]*)";
 var regex = new RegExp( regexS );
 var results = regex.exec( url );
 return results == null ? null : results[1];
}
