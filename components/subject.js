var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        Test: [
            {age : '10'},
            {age : '20'},
            {age : '30'}
        ],
    },
    methods: {
        testVue: function () {
            var myObj = { name: "John", age: 31, city: "New York" };
            var myJSON = JSON.stringify(myObj);
            var that = this
            // that.Test = myJSON.name;
            console.log(this.Test);
        }
    },

})